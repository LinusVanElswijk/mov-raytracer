you can find some renderings on my new blog http://gracefuldegradation.net/
to make your own rendering just position the camera (mouse + arrows or WASD) and press "R"
look in the ../Application/Raytracer/Raytracer folder for the result (a bitmap named "scene_<n>.bmp")
to get better results choose a higher valuer for the AADEPTH parameter in the main file
the code in the "common" folder is not mine, although I partially modified it, to adapt it to my needs

I'm still working on this, one missing key feature is fast collision detection through data acceleration structures (for instance Octree or kD-tree)