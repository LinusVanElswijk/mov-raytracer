#ifndef _OBJECT_H
#define _OBJECT_H

#include "ray.h"
#include "color.h"
#include <glm\glm.hpp>
using glm::dvec3;


struct Material
{
	Color color = Color();
	double diff_coff = 0.5;
	double refl_coff = 0.5;
	double emiss_coff = 0;
	int refl_pow = 10;

	Material(){};
	Material(Color col, double c_diff, double c_refl, int pow){
		diff_coff = c_diff;
		refl_coff = c_refl;
		refl_pow = pow;
		color = col;
	}
	Material(Color col, double c_diff, double c_refl, double c_emiss, int pow){
		diff_coff = c_diff;
		refl_coff = c_refl;
		emiss_coff = c_emiss;
		refl_pow = pow;
		color = col;
	}
};

class Object{
	
public:

	Material material;
	Object();
	Object(Material mat);


	//method functions
	virtual Color getColor();
	virtual double findIntersection(Ray ray);
	virtual dvec3 getNormalAt(dvec3 point);
};

#endif // !OBJECT_H

