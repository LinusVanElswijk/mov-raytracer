#ifndef _COLOR_H
#define _COLOR_H

#include "math.h"

class Color
{
	double red, green, blue, special;

public:
	Color();
	Color(double r, double g, double b, double spec);

	//method functions
	double getColorRed();
	double getColorGreen();
	double getColorBlue();
	double getColorSpecial();

	void setColorRed(double r);
	void setColorGreen(double g);
	void setColorBlue(double b);
	void setColorSpecial(double spec);

	double brightness();
	Color colorScalar(double scalar);
	Color colorAdd(Color color);
	Color colorMult(Color color);
	Color colorAverage(Color color);
	Color clip();
};

#endif // !COLOR_H
