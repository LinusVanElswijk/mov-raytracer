#include "light_surface.h"

dvec3 LightSurface::computeNormal(){
	dvec3 normal = normalize(cross(edge1, edge2));
	return normal;
}

LightSurface::LightSurface(dvec3 point0, dvec3 point1, dvec3 point2, Color col){
	edge1 = point1 - point0;
	edge2 = point2 - point0;
	material.color = col;
	tri1 = Triangle(point0, point1, point2, col);
	tri2 = Triangle(point1, point0 + (edge1 + edge2), point2, col);
	normal = computeNormal();
}

LightSurface::LightSurface(dvec3 point0, dvec3 point1, dvec3 point2, Material mat){
	edge1 = point1 - point0;
	edge2 = point2 - point0;
	material = mat;
	tri1 = Triangle(point0, point1, point2, material.color);
	tri2 = Triangle(point1, point0 + (edge1 + edge2), point2, material.color);
	normal = computeNormal();
}

dvec3 LightSurface::getEdge1(){ return edge1; }
dvec3 LightSurface::getEdge2(){ return edge2; }

dvec3 LightSurface::sampleLight(){
	double random1 = ((double)rand()) / (double)RAND_MAX;
	double random2 = ((double)rand()) / (double)RAND_MAX;
	return (edge1*random1 + edge2*random2) + tri1.getV1();
}

Color LightSurface::getColor(){
	return material.color;
}

Color LightSurface::getLightColor(){
	return material.color;
}

dvec3 LightSurface::getLightPosition(){
	return sampleLight();
}

dvec3 LightSurface::getNormalAt(dvec3 point){
	return normal;
}

double LightSurface::findIntersection(Ray ray){
	double tmp = tri1.findIntersection(ray);
	if (tmp != -1) return tmp;
	else return tri2.findIntersection(ray);
}
