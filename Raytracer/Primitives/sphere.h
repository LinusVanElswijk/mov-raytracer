#ifndef _SPHERE_H
#define _SPHERE_H


#include "color.h"
#include "object.h"
#include <glm\glm.hpp>
using namespace glm;

class Sphere: public Object {
	dvec3 center;
	double radius;

public:
	Sphere();
	Sphere(dvec3 ctr, double rad, Color col);
	Sphere(dvec3 ctr, double rad, Material mat);


	//method functions
	dvec3 getSphereCenter();
	double getSphereRadius();
	Color getColor();

	dvec3 getNormalAt(dvec3 point);

	double findIntersection(Ray ray);

};

#endif // !SPHERE_H


