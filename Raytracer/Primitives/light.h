#ifndef _LIGHT_H
#define _LIGHT_H


#include "color.h"
#include "source.h"

class Light: public Source
{
	dvec3 position;
	Color color;

public:
	Light();
	Light(dvec3 pos, Color col);

	//method functions
	dvec3 getLightPosition();
	Color getLightColor();
	void setLightPosition(dvec3 pos);
	void setLightColor(Color col);

};

#endif // !LIGHT_H

