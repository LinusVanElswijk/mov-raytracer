#ifndef _SOURCE_H
#define _SOURCE_H

#include "color.h"
#include "object.h"
#include <glm\glm.hpp>
using glm::dvec3;

class Source : public Object{
public:

	virtual dvec3 getLightPosition();
	virtual Color getLightColor();
};

#endif //!SOURCE_H