#ifndef _RAY_H
#define _RAY_H

#include "math.h"
#include <glm\glm.hpp>
using glm::dvec3;

class Ray
{
	dvec3 origin, direction;

public:
	Ray();
	Ray(dvec3 origin, dvec3 direction);

	//method functions
	dvec3 getRayOrigin();
	dvec3 getRayDirection();

};

#endif // !RAY_H
