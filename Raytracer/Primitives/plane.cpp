#include "plane.h"

Plane::Plane(){
	normal = dvec3(0, 1, 0);
	point = dvec3(0, 0, 0);
}

Plane::Plane(dvec3 norm, dvec3 pt, Color col){
	normal = norm;
	point = pt;
}

Plane::Plane(dvec3 norm, dvec3 pt, Material mat){
	normal = norm;
	point = pt;
	material = mat;
}

dvec3 Plane::getPlaneNormal(){ return normal; }
dvec3 Plane::getPlanePoint(){ return point; }
Color Plane::getColor(){ return material.color; }
dvec3 Plane::getNormalAt(dvec3 point){ return normal; }
double Plane::findIntersection(Ray ray){ 
	dvec3 ray_direction = ray.getRayDirection();
	double denom = dot(ray_direction, normal);
	if (denom == 0)
		//ray is parallel to plane
		return -1;
	else{
		//P(t) = E + tD, t >= 0 ray parametric equation
		//N(P-Q) = 0, N = norm, Q = point, P = every other point on the plane
		//N(Q - E)/(ND) = t, t < 0, no intersection, t >= 0 => E +tD is the point of intersection
		double numer = dot(normal, (point - ray.getRayOrigin()));
		double t = numer / denom;
		if (t > ACCURACY) return t;
		else return -1;
	}
}