#include "ray.h"

Ray::Ray(){
	origin = dvec3();
	direction = dvec3(1,0,0);
}

Ray::Ray(dvec3 _origin, dvec3 _direction){
	origin = _origin;
	direction = _direction;
}

dvec3 Ray::getRayOrigin(){ return origin; }
dvec3 Ray::getRayDirection(){ return direction; }
