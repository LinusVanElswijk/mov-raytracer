#ifndef _TRIANGLE_H
#define _TRIANGLE_H


#include "color.h"
#include "object.h"
#include "../shared_constants.h"

#include <glm\glm.hpp>
using namespace glm;

class Triangle : public Object {

	dvec3 A, B, C, normal;

public:

	Triangle();
	Triangle(dvec3 a, dvec3 b, dvec3 c, Color col);
	Triangle(dvec3 a, dvec3 b, dvec3 c, dvec3 norm, Color col);
	Triangle(dvec3 a, dvec3 b, dvec3 c, Material mat);

	//method functions
	dvec3 getV1();
	dvec3 getV2();
	dvec3 getV3();
	dvec3 getTriangleNormal();
	double getTriangleDistance();
	Color getColor();

	dvec3 getNormalAt(dvec3 point);
	double findIntersection(Ray ray);

private:
	dvec3 computeNormal();	
	void barycentric(dvec3 P, double* u, double* v, double* w);
};

#endif // !TRIANGLE_H
