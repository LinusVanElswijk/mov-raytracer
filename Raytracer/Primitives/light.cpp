#include "light.h"

Light::Light(){
	position = dvec3(0, 0, 0);
	color = Color(1, 1, 1, 0);
}

Light::Light(dvec3 pos, Color col){
	position = pos;
	color = col;
}

dvec3 Light::getLightPosition(){ return position; }
Color Light::getLightColor(){ return color; }
void Light::setLightPosition(dvec3 pos){ position = pos; }
void Light::setLightColor(Color col){ color = col; }
