#include "sphere.h"
#include <shared_constants.h>

Sphere::Sphere(){
	center = dvec3();
	radius = 1;
	material.color = Color(0.5, 0.5, 0.5, 0);
}

Sphere::Sphere(dvec3 ctr, double rad, Color col){
	center = ctr;
	radius = rad;
	material.color = col;
}

Sphere::Sphere(dvec3 ctr, double rad, Material mat){
	center = ctr;
	radius = rad;
	material = mat;
}


dvec3 Sphere::getSphereCenter(){ return center; }
double Sphere::getSphereRadius(){ return radius; }
Color Sphere::getColor(){ return material.color; }


dvec3 Sphere::getNormalAt(dvec3 point){
	//normal always points from the center of a sphere
	dvec3 normal_vect = normalize(point - center);
	return normal_vect;
}
double Sphere::findIntersection(Ray ray){
	dvec3 ray_origin = ray.getRayOrigin();
	double ray_origin_x = ray_origin.x;
	double ray_origin_y = ray_origin.y;
	double ray_origin_z = ray_origin.z;

	dvec3 ray_direction = ray.getRayDirection();
	double ray_direction_x = ray_direction.x;
	double ray_direction_y = ray_direction.y;
	double ray_direction_z = ray_direction.z;

	dvec3 sphere_center = center;
	double sphere_center_x = sphere_center.x;
	double sphere_center_y = sphere_center.y;
	double sphere_center_z = sphere_center.z;

	double a = 1; //normalized
	double b =	(2 * (ray_origin_x - sphere_center_x)*ray_direction_x) +
				(2 * (ray_origin_y - sphere_center_y)*ray_direction_y) +
				(2 * (ray_origin_z - sphere_center_z)*ray_direction_z);
	double c =	pow(ray_origin_x - sphere_center_x, 2) +
				pow(ray_origin_y - sphere_center_y, 2) +
				pow(ray_origin_z - sphere_center_z, 2) -
				(radius*radius);
	double discriminant = b*b - 4 * c;

	if (discriminant > 0){
		//the ray intersects the sphere

		//first root
		double root_1 = ((-1 * b - sqrt(discriminant)) / 2);
		if (root_1 > ACCURACY) return root_1;
		else{
			double root_2 = ((sqrt(discriminant) - b) / 2);
			if (root_2 > ACCURACY) return root_2;
		}
		return -1;
	}
	else return -1; //the ray misses the sphere
}
