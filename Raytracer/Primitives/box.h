#pragma once
#include <glm\vec3.hpp>
#include <Primitives\ray.h>
#include <Primitives\object.h>
#include <Primitives\triangle.h>
#include <shared_constants.h>
#include <vector>
using glm::dvec3;
using std::vector;

class Box
{
private:
	dvec3 bounds[2];
public:
	Box(double min_x, double max_x, double min_y, double max_y, double min_z, double max_z);
	Box(dvec3 vec_min, dvec3 vec_max);
	bool boxRayIntersect(Ray ray, double* t_min, double* t_max);
	bool isPointInBox(dvec3 point);
	static Box* findContainingBox(vector<Object*> scene_objects);
	Box* topLeftForward();
	Box* topRightForward();
	Box* topLeftBackward();
	Box* topRightBackward();
	Box* bottomLeftForward();
	Box* bottomRightForward();
	Box* bottomLeftBackward();
	Box* bottomRightBackward();
};