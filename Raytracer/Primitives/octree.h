#pragma once
#include <vector>
#include <glm\glm.hpp>
#include <Primitives\box.h>
#include <Primitives\triangle.h>
#include <Primitives\object.h>
#include <vector>

using glm::dvec3;
using std::vector;

class Octree
{

public:
	Octree(Box* box);
	Octree(unsigned short depth, Box* initial_box);
	Octree* findChild(dvec3 point);
	void insertTriangle(Triangle triangle, int index);
	void minIntersect(vector<Object*> scene_objects, Ray ray, double* min_intersection, int* index);
private:
	std::vector<int> indices;
	Box* box;
	Octree* children[8];
	short whichChild(dvec3 point);
};

