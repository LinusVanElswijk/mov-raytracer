#include "box.h"


Box::Box(double min_x, double max_x, double min_y, double max_y, double min_z, double max_z){
	bounds[0] = dvec3(min_x, min_y, min_z);
	bounds[1] = dvec3(max_x, max_y, max_z);
}

Box::Box(dvec3 vec_min, dvec3 vec_max){
	bounds[0] = vec_min;
	bounds[1] = vec_max;
}

bool Box::boxRayIntersect(Ray ray, double* t_min, double* t_max){
	int sign[3];
	dvec3 orig = ray.getRayOrigin();
	dvec3 dir = ray.getRayDirection();	
	dvec3 invdir = 1.0 / dir;
	sign[0] = (invdir.x < 0);
	sign[1] = (invdir.y < 0);
	sign[2] = (invdir.z < 0);
	double tmin, tmax, tymin, tymax, tzmin, tzmax;
	tmin = (bounds[sign[0]].x - orig.x) * invdir.x;
	tmax = (bounds[1 - sign[0]].x - orig.x) * invdir.x;
	tymin = (bounds[sign[1]].y - orig.y) * invdir.y;
	tymax = (bounds[1 - sign[1]].y - orig.y) * invdir.y;
	if ((tmin > tymax) || (tymin > tmax))
		return false;
	if (tymin > tmin)
		tmin = tymin;
	if (tymax < tmax)
		tmax = tymax;
	tzmin = (bounds[sign[2]].z - orig.z) * invdir.z;
	tzmax = (bounds[1 - sign[2]].z - orig.z) * invdir.z;
	if ((tmin > tzmax) || (tzmin > tmax))
		return false;
	if (tzmin > tmin)
		tmin = tzmin;
	if (tzmax < tmax)
		tmax = tzmax;
	if (tmin < 0){
		*t_min = -1;
		*t_max = -1;
		return false;
	}
	else{
		*t_min = tmin;
		*t_max = tmax;
		return true;
	}	
}



bool Box::isPointInBox(dvec3 point){
	bool cond_1 = point.x >= bounds[0].x && point.x < bounds[1].x;
	bool cond_2 = point.y >= bounds[0].y && point.y < bounds[1].y;
	bool cond_3 = point.z >= bounds[0].z && point.z < bounds[1].z;
	return cond_1 && cond_2 && cond_3;
}

Box* Box::topLeftForward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xmin, yhalf, zhalf);
	dvec3 new_box_max = dvec3(xhalf, ymax, zmax);
	return new Box(new_box_min, new_box_max);
}
Box* Box::topRightForward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xhalf, yhalf, zhalf);
	dvec3 new_box_max = dvec3(xmax, ymax, zmax);
	return new Box(new_box_min, new_box_max);
}
Box* Box::topLeftBackward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xmin, yhalf, zmin);
	dvec3 new_box_max = dvec3(xhalf, ymax, zhalf);
	return new Box(new_box_min, new_box_max);
}
Box* Box::topRightBackward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xhalf, yhalf, zmin);
	dvec3 new_box_max = dvec3(xmax, ymax, zhalf);
	return new Box(new_box_min, new_box_max);
}
Box* Box::bottomLeftForward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xmin, ymin, zhalf);
	dvec3 new_box_max = dvec3(xhalf, yhalf, zmax);
	return new Box(new_box_min, new_box_max);
}
Box* Box::bottomRightForward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xhalf, ymin, zhalf);
	dvec3 new_box_max = dvec3(xmax, yhalf, zmax);
	return new Box(new_box_min, new_box_max);
}
Box* Box::bottomLeftBackward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xmin, ymin, zmin);
	dvec3 new_box_max = dvec3(xhalf, yhalf, zhalf);
	return new Box(new_box_min, new_box_max);
}
Box* Box::bottomRightBackward(){
	double xmin = bounds[0].x;
	double ymin = bounds[0].y;
	double zmin = bounds[0].z;
	double xmax = bounds[1].x;
	double ymax = bounds[1].y;
	double zmax = bounds[1].z;
	double xhalf = bounds[0].x + (bounds[1].x - bounds[0].x) / 2;
	double yhalf = bounds[0].y + (bounds[1].y - bounds[0].y) / 2;
	double zhalf = bounds[0].z + (bounds[1].z - bounds[0].z) / 2;
	dvec3 new_box_min = dvec3(xhalf, ymin, zmin);
	dvec3 new_box_max = dvec3(xmax, yhalf, zhalf);
	return new Box(new_box_min, new_box_max);
}

Box* Box::findContainingBox(vector<Object*> scene_objects){
	double minx = DBL_MAX;
	double miny = DBL_MAX;
	double minz = DBL_MAX;
	double maxx = DBL_MIN;
	double maxy = DBL_MIN;
	double maxz = DBL_MIN;
	for (int i = 0; i < (int)scene_objects.size(); i++){
		Triangle* tri = dynamic_cast<Triangle*>(scene_objects[i]);
		if (tri != NULL){
			if (minx > tri->getV1().x) minx = tri->getV1().x;
			if (miny > tri->getV1().y) miny = tri->getV1().y;
			if (minz > tri->getV1().z) minz = tri->getV1().z;
			if (minx > tri->getV2().x) minx = tri->getV2().x;
			if (miny > tri->getV2().y) miny = tri->getV2().y;
			if (minz > tri->getV2().z) minz = tri->getV2().z;
			if (minx > tri->getV3().x) minx = tri->getV3().x;
			if (miny > tri->getV3().y) miny = tri->getV3().y;
			if (minz > tri->getV3().z) minz = tri->getV3().z;
			if (maxx < tri->getV1().x) maxx = tri->getV1().x;
			if (maxy < tri->getV1().y) maxy = tri->getV1().y;
			if (maxz < tri->getV1().z) maxz = tri->getV1().z;
			if (maxx < tri->getV2().x) maxx = tri->getV2().x;
			if (maxy < tri->getV2().y) maxy = tri->getV2().y;
			if (maxz < tri->getV2().z) maxz = tri->getV2().z;
			if (maxx < tri->getV3().x) maxx = tri->getV3().x;
			if (maxy < tri->getV3().y) maxy = tri->getV3().y;
			if (maxz < tri->getV3().z) maxz = tri->getV3().z;
		}
	}
	return new Box(minx - ACCURACY, miny - ACCURACY, minz - ACCURACY, maxx + ACCURACY, maxy + ACCURACY, maxz + ACCURACY);
}