#include "triangle.h"

void Triangle::barycentric(dvec3 P, double* u, double* v, double* w){
	dvec3 v0 = B - A, v1 = C - A, v2 = P - A;
	double d00 = dot(v0, v0);
	double d01 = dot(v0, v1);
	double d11 = dot(v1, v1);
	double d21 = dot(v2, v1);
	double d20 = dot(v2, v0);
	double denom = d00 * d11 - d01 * d01;
	*v = (d11 * d20 - d01 * d21) / denom;
	*w = (d00 * d21 - d01 * d20) / denom;
	*u = 1.0f - *v - *w;
}


dvec3 Triangle::computeNormal(){
	dvec3 CA(C - A);
	dvec3 BA(B - A);
	dvec3 normal = normalize(cross(BA, CA));
	return normal;
}

Triangle::Triangle(){
	A = dvec3(1, 0, 0);
	B = dvec3(0, 1, 0);
	C = dvec3(0, 0, 1);
	normal = computeNormal();
	material.color = Color();
	
}

Triangle::Triangle(dvec3 a, dvec3 b, dvec3 c, Color col){
	A = a;
	B = b;
	C = c;
	material.color = col;
	normal = computeNormal();

}

Triangle::Triangle(dvec3 a, dvec3 b, dvec3 c, dvec3 norm, Color col){
	A = a;
	B = b;
	C = c;
	material.color = col;
	normal = norm;

}

Triangle::Triangle(dvec3 a, dvec3 b, dvec3 c, Material mat){
	A = a;
	B = b;
	C = c;
	normal = computeNormal();
	material = mat;
}

dvec3 Triangle::getV1(){return A;}
dvec3 Triangle::getV2(){return B;}
dvec3 Triangle::getV3(){return C;}

dvec3 Triangle::getTriangleNormal(){ 
	return normal; 
}
double Triangle::getTriangleDistance(){ 
	dvec3 normal = getTriangleNormal();
	double distance = dot(normal, A);
	return distance; 
}

Color Triangle::getColor(){ return material.color; }

dvec3 Triangle::getNormalAt(dvec3 point){ 
	return getTriangleNormal(); 
}

double Triangle::findIntersection(Ray ray){
	dvec3 ray_direction = ray.getRayDirection();
	dvec3 ray_origin = ray.getRayOrigin();

	dvec3 edge1 = B - A;
	dvec3 edge2 = C - A;
	dvec3 pvec = cross(ray_direction, edge2);
	double det = dot(edge1, pvec);
	if (det > -ACCURACY && det < ACCURACY) return -1;
	double invDet = 1 / det;
	dvec3 tvec = ray_origin - A;
	double u = dot(tvec,pvec) * invDet;
	if ( u < 0 || u > 1) return -1;
	dvec3 qvec = cross(tvec, edge1);
	double v = dot(ray_direction, qvec) * invDet;
	if (v < 0 || u + v > 1) return -1;
	double t = dot(edge2, qvec) * invDet;
	if(t > ACCURACY) return t;
	return -1;
}
