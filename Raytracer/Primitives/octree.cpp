#include "octree.h"


Octree::Octree(Box* box){
	this->box = box;
	for (int i = 0; i < 8; i++) children[i] = NULL;
}

Octree::Octree(unsigned short depth, Box* initial_box){
	if (depth > 0){
		depth--;
		children[0] = new Octree(initial_box->topLeftForward());
		children[1] = new Octree(initial_box->topRightForward());
		children[2] = new Octree(initial_box->topLeftBackward());
		children[3] = new Octree(initial_box->topRightBackward());
		children[4] = new Octree(initial_box->bottomLeftForward());
		children[5] = new Octree(initial_box->bottomRightForward());
		children[6] = new Octree(initial_box->bottomLeftBackward());
		children[7] = new Octree(initial_box->bottomRightBackward());
	}
}

short Octree::whichChild(dvec3 point){
	short i;
	bool found = false;
	for (i = 0; i < 8; i++) 
		if (children[i]->box->isPointInBox(point)){
			found = true;
			break;
		}
	if (found == true) return i;
	return -1;
}

Octree* Octree::findChild(dvec3 point){
	Octree* oct;
	if (children[0] == NULL) return this;
	else{
		short i = whichChild(point);
		if (i == -1) return NULL;
		oct = children[i];
		return oct->findChild(point);
	}

}

void Octree::insertTriangle(Triangle triangle, int index){
	dvec3 p1 = triangle.getV1();
	dvec3 p2 = triangle.getV2();
	dvec3 p3 = triangle.getV3();
	Octree* oct = findChild(p1);
	if (oct != NULL && (int)oct->indices.size() != 0 && oct->indices[(int)oct->indices.size() - 1] != index) oct->indices.push_back(index);
	oct = findChild(p2);
	if ((int)oct->indices.size() != 0 && oct->indices[(int)oct->indices.size() - 1] != index) oct->indices.push_back(index);
	oct = findChild(p3);
	if ((int)oct->indices.size() != 0 && oct->indices[(int)oct->indices.size() - 1] != index) oct->indices.push_back(index);
}

void Octree::minIntersect(vector<Object*> scene_objects, Ray ray, double* min_intersection, int* index){
	*index = -1;
	*min_intersection = -1;
	double tmin, tmax;
	Octree* oct = NULL;
	if (box->boxRayIntersect(ray, &tmin, &tmax)) oct = findChild(dvec3(ray.getRayDirection()*tmin));
	while (oct != NULL && oct->box->boxRayIntersect(ray, &tmin, &tmax)){
		for (int i = 0; i < (int)oct->indices.size(); i++){
			*min_intersection = (dynamic_cast<Triangle*>(scene_objects.at(indices[i])))->findIntersection(ray);
			if (*min_intersection != -1){
				*index = indices[i];
				break;
			}
		}
		if (*min_intersection != -1) break;
		oct = findChild(dvec3(ray.getRayDirection()*tmax));
	}
}


