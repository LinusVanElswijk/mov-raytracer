#include "vect.h"

Vect::Vect(){
	x = 0;
	y = 0;
	z = 0;
}

Vect::Vect(double _x, double _y, double _z){
	x = _x;
	y = _y;
	z = _z;
}

double Vect::getVectX(){ return x; }
double Vect::getVectY(){ return y; }
double Vect::getVectZ(){ return z; }

double Vect::magnitude(){ return sqrt(x*x + y*y + z*z); }

double Vect::dotProduct(Vect v){ return x*v.getVectX() + y*v.getVectY() + z*v.getVectZ(); }

Vect Vect::crossProduct(Vect v){
	return Vect(y*v.getVectZ() - z*v.getVectY(), z*v.getVectX() - x*v.getVectZ(), x*v.getVectY() - y*v.getVectX());
}

Vect Vect::normalize(){ 
	double magnitude = Vect::magnitude();
	return Vect(x / magnitude, y / magnitude, z / magnitude); 
}

Vect Vect::negative(){	return Vect(-x, -y, -z); }
Vect Vect::vectAdd(Vect v){ return Vect(x+v.getVectX(), y+v.getVectY(), z+v.getVectZ()); }
Vect Vect::vectDiff(Vect v){ return vectAdd(v.negative()); }
Vect Vect::vectMult(double scalar){ return Vect(x*scalar, y*scalar, z*scalar); }
