#ifndef _LIGHT_SURFACE_H
#define _LIGHT_SURFACE_H


#include "color.h"
#include "object.h"
#include "source.h"
#include "triangle.h"

#include "../shared_constants.h"

#include <glm\glm.hpp>
using namespace glm;

class LightSurface : public Source {

	dvec3 edge1, edge2, normal;

public:

	//LightSurface();
	//Triangle(dvec3 a, dvec3 b, dvec3 c, Color col);
	//Triangle(dvec3 a, dvec3 b, dvec3 c, dvec3 norm, Color col);
	LightSurface(dvec3 point0, dvec3 point1, dvec3 point2, Color col);
	LightSurface(dvec3 point0, dvec3 point1, dvec3 point2, Material mat);

	//method functions
	dvec3 getEdge1();
	dvec3 getEdge2();
	dvec3 getLightPosition();
	Color getLightColor();
	dvec3 sampleLight();

	Color getColor();
	

	dvec3 getNormalAt(dvec3 point);
	double findIntersection(Ray ray);

private:
	Triangle tri1, tri2;
	dvec3 computeNormal();
};

#endif // !_LIGHT_SURFACE_H
