#ifndef _PLANE_H
#define _PLANE_H

#include "color.h"
#include "object.h"
#include "../shared_constants.h"
#include <glm\glm.hpp>
using namespace glm;

class Plane : public Object {	
	
	dvec3 normal;	
	dvec3 point;

public:
	Plane();
	Plane(dvec3 norm, dvec3 pt, Color col);
	Plane(dvec3 norm, dvec3 pt, Material mat);


	//method functions
	dvec3 getPlaneNormal();
	dvec3 getPlanePoint();
	Color getColor();

	dvec3 getNormalAt(dvec3 point);
	double findIntersection(Ray ray);
};

#endif // !PLANE_H