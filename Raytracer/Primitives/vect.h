#ifndef _VECT_H
#define _VECT_H

#include "math.h"

class Vect
{
	double x, y, z;

public:
	Vect();
	Vect(double x, double y, double z);

	//method functions
	double getVectX();
	double getVectY();
	double getVectZ();

	double magnitude();	
	double dotProduct(Vect v);
	
	Vect crossProduct(Vect v);

	Vect normalize();
	Vect negative();
	
	Vect vectAdd(Vect v);
	Vect vectDiff(Vect v);
	Vect vectMult(double scalar);
};

#endif // !VECT_H
