#include <common\camera.h>

Camera::Camera(){
	position = vec3(0, 0.8, 5);
	up = vec3(0, 1, 0);
	direction = vec3(0, 0, -1);
	right = vec3(1, 0, 0);
}

Camera::Camera(vec3 pos, vec3 target){
	position = vec3(0, 0, 5);
	up = vec3(0, 1, 0);
	direction = normalize(pos - target);
	right = normalize(cross(direction, vec3(0,1,0)));
	up = normalize(cross(right, direction));
}