#ifndef CONTROLS_HPP
#define CONTROLS_HPP

#include <common\camera.h>

void computeMatricesFromInputs(Camera* cam, GLFWwindow* window);
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();

#endif