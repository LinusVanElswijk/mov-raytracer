#pragma once

#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

class Camera
{
public:
	vec3 position;
	vec3 direction;
	vec3 right;
	vec3 up;

	Camera();
	Camera(vec3 pos, vec3 target);
	
};