#version 430

in layout(location=0) vec3 vertexPosition_modelspace;
in layout(location=1) vec3 vertexColor;
out vec3 clr;

uniform mat4 MVP;
 
void main(){
 
    // Output position of the vertex, in clip space : MVP * position
    vec4 v = vec4(vertexPosition_modelspace,1); // Transform an homogeneous 4D vector, remember ?
    gl_Position = MVP * v;
	clr = vertexColor;
}