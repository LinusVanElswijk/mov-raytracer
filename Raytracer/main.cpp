#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <limits>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <shared_constants.h>

GLFWwindow* window;

// Include GLM
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\rotate_vector.hpp>
using glm::vec3;
using glm::mat4;

#include <common\shader.hpp>
#include <common\texture.hpp>
#include <common\controls.hpp>
#include <common\objloader.hpp>
#include <common\camera.h>


#include <Primitives\ray.h>
#include <Primitives\color.h>
#include <Primitives\source.h>
#include <Primitives\light.h>
#include <Primitives\object.h>
#include <Primitives\sphere.h>
#include <Primitives\plane.h>
#include <Primitives\triangle.h>
#include <Primitives\box.h>
#include <Primitives\octree.h>
#include <Primitives\light_surface.h>

#define _USE_MATH_DEFINES
enum AliasTechnique{ SIMPLE_GRID, JITTERED_GRID, RANDOM, GAUSSIAN };


#include <math.h>

using namespace std;

const double ALPHA = M_PI/3;
const double COTAN = 1/tan(ALPHA / 2);
const unsigned int WIDTH = 1024;
const unsigned int HEIGHT = 768;
const unsigned short AADEPTH = 4;
const double AMBIENT_LIGHT = 0.0;
const short RECURSION_DEPTH = 50;
const double SIGMA_SQUARED = 1;
const short SHADOW_BRANCH = 1;
const short REFLECTION_BRANCH = 5;
const double OVERLAP = 0;
const AliasTechnique TECHNIQUE = RANDOM;


int glInit(){

	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(1024, 768, "Advanced Graphics - Second Practical - Spartan", NULL, NULL);
	if (window == NULL){
		fprintf(stderr, "Failed to open GLFW window\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	return 0;
}

struct RGBType{
	double r;
	double g;
	double b;
};

//copied from the internet, not my code
void savebmp(const char *filename, int w, int h, int dpi, RGBType *data){
	FILE* f;
	int k = w*h;
	int s = 4 * k;

	int filesize = 54 + s;
	double factor = 39.375;
	int m = static_cast<int>(factor);

	int ppm = dpi*m;

	unsigned char bmpfileheader[14] = { 'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0 };
	unsigned char bmpinfoheader[40] = { 40,0,0,0, 0,0,0,0, 0,0,0,0, 1, 0, 24, 0 };

	bmpfileheader[2] = (unsigned char)(filesize);
	bmpfileheader[3] = (unsigned char)(filesize>>8);
	bmpfileheader[4] = (unsigned char)(filesize>>16);
	bmpfileheader[5] = (unsigned char)(filesize>>24);

	bmpinfoheader[4] = (unsigned char)(w);
	bmpinfoheader[5] = (unsigned char)(w>>8);
	bmpinfoheader[6] = (unsigned char)(w>>16);
	bmpinfoheader[7] = (unsigned char)(w>>24);

	bmpinfoheader[8] = (unsigned char)(h);
	bmpinfoheader[9] = (unsigned char)(h >> 8);
	bmpinfoheader[10] = (unsigned char)(h >> 16);
	bmpinfoheader[11] = (unsigned char)(h >> 24);

	bmpinfoheader[21] = (unsigned char)(s);
	bmpinfoheader[22] = (unsigned char)(s >> 8);
	bmpinfoheader[23] = (unsigned char)(s >> 16);
	bmpinfoheader[24] = (unsigned char)(s >> 24);

	bmpinfoheader[25] = (unsigned char)(ppm);
	bmpinfoheader[26] = (unsigned char)(ppm >> 8);
	bmpinfoheader[27] = (unsigned char)(ppm >> 16);
	bmpinfoheader[28] = (unsigned char)(ppm >> 24);

	bmpinfoheader[29] = (unsigned char)(ppm);
	bmpinfoheader[30] = (unsigned char)(ppm >> 8);
	bmpinfoheader[31] = (unsigned char)(ppm >> 16);
	bmpinfoheader[32] = (unsigned char)(ppm >> 24);

	fopen_s(&f, filename, "wb");
	fwrite(bmpfileheader, 1, 14, f);
	fwrite(bmpinfoheader, 1, 40, f);

	for (int i = 0; i < k; i++){
		RGBType rgb = data[i];

		double red = (data[i].r)*255;
		double green = (data[i].g) * 255;
		double blue = (data[i].b) * 255;

		unsigned char color[3] = { (int)floor(blue), (int)floor(green), (int)floor(red) };

		fwrite(color, 1, 3, f);
	}
	fclose(f);
}

//finds the closes object that collides with the ray
//to be improved by using data acceleration structures
int winningObjectIndex(vector<double> object_intersections){
	int index_of_min = -1;
	if (object_intersections.size() == 0) return -1;	
	for (int i = 0; i < (int)object_intersections.size(); i++){
		if (i == 0){
			if (object_intersections[0] >= 0) index_of_min = 0;
			else index_of_min = -1;
		}		
		else if (object_intersections[i] >= 0){
			if (index_of_min == -1) index_of_min = i;
			else if (object_intersections[i] < object_intersections[index_of_min]) index_of_min = i;
		}
	}
	return index_of_min;
}

//A point on a ray is represented as ray.origin + ray.direction*t
//this function finds the t parameter for the winning object
void findMinIntersect(vector<Object*> scene_objects, Ray ray, double* min_intersect, int* index_of_winning_object){
	*index_of_winning_object = -1;
	*min_intersect = -1;
	for (int i = 0; i < (int)scene_objects.size(); i++){
		double intersect = -1;
		intersect = scene_objects[i]->findIntersection(ray);
		if (intersect != -1){
			if (*min_intersect == -1){
				*min_intersect = intersect;
				*index_of_winning_object = i;
			}
			else if (*min_intersect > intersect){
				*min_intersect = intersect;
				*index_of_winning_object = i;
			}
		}
	}
}

//finds any collision
bool shadowIntersect(vector<Object*> scene_objects, Ray ray, double vector_to_light_magnitude){
	for (int i = 0; i < (int)scene_objects.size(); i++){
		double intersect = -1;
		intersect = scene_objects[i]->findIntersection(ray);
		if (intersect != -1 && intersect < vector_to_light_magnitude) return true;
	}
	return false;
}

dvec3 sampleReflRay(dvec3 incoming_ray, dvec3 reflected_ray, dvec3 normal, Material mat, double *p){
	double rnd1 = (double)rand() / RAND_MAX;
	double rnd2 = (double)rand() / RAND_MAX;
	int n = mat.refl_pow;
	double theta = acos(pow(rnd1, (1.0 / (n + 1))));
	double phi = 2 * M_PI * rnd2;
	double dt = dot((-1.0)*incoming_ray, normal);
	*p = dt*((n + 2.0) / (n + 1.0));
	if (*p > 1) *p = 1;
	if (theta > M_PI_2){
		theta = M_PI_2;
		*p = 0;
	}
	dvec3 axis = cross(reflected_ray, normal);
	dvec3 refl_direction = reflected_ray;
	refl_direction = glm::rotate(refl_direction, theta, axis);
	refl_direction = glm::rotate(refl_direction, phi, reflected_ray);
	refl_direction = normalize(refl_direction);
	if (dot(refl_direction, normal) < 0) *p = 0;
	return refl_direction;
}


dvec3 sampleDiffRay(dvec3 incoming_ray, dvec3 reflected_ray, dvec3 normal, Material mat, double *p){
	double rnd1 = (double)rand() / RAND_MAX;
	double rnd2 = (double)rand() / RAND_MAX;
	int n = mat.refl_pow;
	double theta = acos(sqrt(rnd1));
	double phi = 2 * M_PI * rnd2;
	*p = 1;
	
	dvec3 axis = cross(reflected_ray, normal);
	dvec3 refl_direction = normal;
	refl_direction = glm::rotate(refl_direction, theta, axis);
	refl_direction = glm::rotate(refl_direction, phi, normal);
	refl_direction = normalize(refl_direction);
	if (dot(refl_direction, normal) < 0) *p = 0;
	return refl_direction;
}

Color getColorAt(dvec3 intersection_position, dvec3 intersection_ray_direction, vector<Object*> scene_objects, int index_of_winning_object, vector<Source*> light_sources, double ambientLight, short recursion_depth){
	if (recursion_depth < 0) return Color(0, 0, 0, 0);
	recursion_depth--;
	Color winning_object_color = scene_objects[index_of_winning_object]->getColor();
	Material winning_object_material = scene_objects[index_of_winning_object]->material;
	dvec3 winning_object_normal = scene_objects[index_of_winning_object]->getNormalAt(intersection_position);

	if (winning_object_color.getColorSpecial() == 2) {
		//checked/tile floor pattern
		int square = (int)floor(intersection_position.x*5) + (int)floor(intersection_position.z*5);
		if ((square % 2) == 0){
			winning_object_color.setColorRed(0);
			winning_object_color.setColorGreen(0);
			winning_object_color.setColorBlue(0);
		}
		else{
			winning_object_color.setColorRed(1);
			winning_object_color.setColorGreen(1);
			winning_object_color.setColorBlue(1);
		}
	}

	Color final_color = winning_object_color.colorScalar(winning_object_material.emiss_coff);
	
	//http://paulbourke.net/geometry/reflected/
	//R = I - 2N(I.N)
	dvec3 real_refl = normalize(intersection_ray_direction - (winning_object_normal * (2 * dot(winning_object_normal, intersection_ray_direction))));
	
	//Russian Roulette
	double rnd = (double)rand() / RAND_MAX;
	if (dot((-1.0)*intersection_ray_direction, winning_object_normal) > 0){		
		if (rnd < winning_object_material.diff_coff){
			double p = 0;		
			dvec3 reflection_direction = sampleDiffRay(intersection_ray_direction, real_refl, winning_object_normal, winning_object_material, &p);
			Ray reflection_ray(intersection_position, reflection_direction);
			int index_of_winning_object_with_reflection;
			double min_reflection_intersect;
			findMinIntersect(scene_objects, reflection_ray, &min_reflection_intersect, &index_of_winning_object_with_reflection);
			if (index_of_winning_object_with_reflection != -1 && scene_objects[index_of_winning_object] != light_sources[0]){
				dvec3 reflection_intersection_position = intersection_position + (reflection_direction *min_reflection_intersect);
				dvec3 reflection_intersection_ray_direction = reflection_direction;
				Color reflection_intersection_color =
					getColorAt(reflection_intersection_position, reflection_intersection_ray_direction, scene_objects, index_of_winning_object_with_reflection, light_sources, ambientLight, recursion_depth);
				final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(p).colorMult(winning_object_color));
			}
		}
		if (rnd >= winning_object_material.diff_coff && rnd < winning_object_material.diff_coff + winning_object_material.refl_coff){
			double p = 0;
			dvec3 reflection_direction = sampleReflRay(intersection_ray_direction, real_refl, winning_object_normal, winning_object_material, &p);
			Ray reflection_ray(intersection_position, reflection_direction);
			int index_of_winning_object_with_reflection;
			double min_reflection_intersect;
			findMinIntersect(scene_objects, reflection_ray, &min_reflection_intersect, &index_of_winning_object_with_reflection);
			if (index_of_winning_object_with_reflection != -1 && scene_objects[index_of_winning_object] != light_sources[0]){
				dvec3 reflection_intersection_position = intersection_position + (reflection_direction *min_reflection_intersect);
				dvec3 reflection_intersection_ray_direction = reflection_direction;
				Color reflection_intersection_color =
					getColorAt(reflection_intersection_position, reflection_intersection_ray_direction, scene_objects, index_of_winning_object_with_reflection, light_sources, ambientLight, recursion_depth);
				final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(p).colorMult(winning_object_color));
			}
		}
		if (rnd >= winning_object_material.diff_coff + winning_object_material.refl_coff) return Color(0, 0, 0, 0);
	}

	LightSurface* light = dynamic_cast<LightSurface*>(light_sources[0]);
	if (scene_objects[index_of_winning_object] != light_sources[0]){
		double area = 2.0*cross(light->getEdge1(), light->getEdge2()).length();
		for (short i = 0; i < SHADOW_BRANCH; i++){
			bool shadowed = false;
			dvec3 sampled_light_point = light->getLightPosition();
			dvec3 vector_to_light = sampled_light_point - intersection_position;
			double vector_to_light_magnitude = vector_to_light.length();
			vector_to_light = normalize(vector_to_light);
			double cosine_angle = dot(winning_object_normal, vector_to_light);
			double cosine_angle_2 = dot(vector_to_light, light->getNormalAt(sampled_light_point));
			if (cosine_angle > ACCURACY){
				bool shadowed = false;
				Ray shadow_ray(intersection_position, vector_to_light);
				shadowed = shadowIntersect(scene_objects, shadow_ray, vector_to_light_magnitude);
				if (shadowed == false){
					final_color =
						final_color.colorAdd(winning_object_color.colorMult(light_sources[0]->getLightColor()).colorScalar(1.0 / SHADOW_BRANCH*cosine_angle*cosine_angle_2));
				}
			}
		}
	}
	return final_color.clip();
}

double randomDouble(double a, double b) {
	double random = ((double)rand()) / (double)RAND_MAX;

	double diff = abs(b - a);
	double r = random * diff;
	return a + r;
}

void aliasedPixel(int x, int y, double* x_aliased, double* y_aliased, int aa_offset_x, int aa_offset_y, AliasTechnique aa_technique){
	switch (aa_technique){
		case(SIMPLE_GRID) :
			*x_aliased = (x + (double)aa_offset_x / AADEPTH + 1 / (2.0*AADEPTH)) - 0.5 * WIDTH;
			*y_aliased = (y + (double)aa_offset_y / AADEPTH + 1 / (2.0*AADEPTH)) - 0.5 * HEIGHT;
			break;
		case(JITTERED_GRID) :
			*x_aliased = randomDouble(x + (double)aa_offset_x / AADEPTH, x + (double)(aa_offset_x + 1) / AADEPTH) - 0.5 * WIDTH;
			*y_aliased = randomDouble(y + (double)aa_offset_y / AADEPTH, y + (double)(aa_offset_y + 1) / AADEPTH) - 0.5 * HEIGHT;
			break;		
		case(RANDOM) :
			*x_aliased = randomDouble(x, x + 1) - 0.5 * WIDTH;
			*y_aliased = randomDouble(y, y + 1) - 0.5 * HEIGHT;
			break;
		case(GAUSSIAN) :
			*x_aliased = randomDouble(x - OVERLAP / 2.0, x + 1 + OVERLAP / 2.0) - 0.5 * WIDTH;
			*y_aliased = randomDouble(y - OVERLAP / 2.0, y + 1 + OVERLAP / 2.0) - 0.5 * HEIGHT;
			break;
		default:
			break;
	}
}



void rayTrace(vector<Object*> scene_objects, Camera camera){
	clock_t t1, t2;
	t1 = clock();
	int dpi = 72;

	double aathreshold = 0.1;

	int n = WIDTH*HEIGHT;
	RGBType *pixels = new RGBType[n];


	dvec3 O(0, 0, 0);
	dvec3 X(1, 0, 0);
	dvec3 Y(0, 1, 0);
	dvec3 Z(0, 0, 1);

	Color white_light(1, 1, 1, 0);
	Color purple_light(0.5, 0.2, 0.5, 0);
	Color pretty_green(0.5, 1, 0.5, 0.5);
	Color pretty_blue(0.5, 0.5, 1, 0.9);
	Color maroon(0.5, 0.25, 0.25, 0);
	Color tile_floor(1, 1, 1, 2);
	Color gray(0.5, 0.5, 0.5, 0);
	Color black(0, 0, 0, 0);
	Color orange(0.94, 0.75, 0.31, 0);
	Color pretty_orange(0.94, 0.75, 0.31, 0.7);

	Material shiny_blue = Material(pretty_blue, 0.2, 0.79, 10000);
	Material shiny_green = Material(pretty_green, 0.2, 0.79, 10000);
	Material shiny = Material(Color(1,1,1,0.5), 0.0, 0.99, 10000);
	Material not_shiny_green = Material(pretty_green, 0.99, 0, 10000);

	vec3 light_position(	camera.position.x + 3,
							camera.position.y + 10, 
							camera.position.z);
	vec3 light_position_2(5, 10, -2);
	Light scene_light(light_position, white_light);
	Light scene_light_2(light_position_2, purple_light);
	vector<Source*> light_sources;
	//light_sources.push_back(dynamic_cast<Source*>(&scene_light));
	//light_sources.push_back(dynamic_cast<Source*>(&scene_light_2));

	//scene objects
	Sphere scene_sphere(O + dvec3(-2.3, -0.2, 0), 0.8, shiny);
	Sphere scene_sphere2(O + dvec3(+2.3, -0.2, 0), 0.8, shiny_blue);
	Triangle scene_triangle(vec3(-5, 0.5, -2), vec3(5, 0.5, -2), vec3(0, 2, 0), pretty_orange);
	Plane scene_plane(Y, dvec3(0, -1, 0), tile_floor);
	LightSurface scene_light_surface(vec3(-2, 3-ACCURACY, -3), vec3(-2, 3-ACCURACY, 1), vec3(2, 3-ACCURACY, -3), Material(Color(1, 1, 1, 0), 0.04, 0, 0.95, 500));
	light_sources.push_back(dynamic_cast<Source*>(&scene_light_surface));
	scene_objects.push_back(dynamic_cast<Object*>(&scene_sphere));
	scene_objects.push_back(dynamic_cast<Object*>(&scene_sphere2));
	//scene_objects.push_back(dynamic_cast<Object*>(&scene_plane));
	//scene_objects.push_back(dynamic_cast<Object*>(&scene_triangle));

	scene_objects.push_back(dynamic_cast<Object*>(&scene_light_surface));


	int thisone, aa_index;
	double xamnt, yamnt;

	for (int x = 0; x < WIDTH; x++){
		for (int y = 0; y < HEIGHT; y++){
			
			thisone = y*WIDTH + x;

			double total_weight = 0;
			double sample_weight[AADEPTH*AADEPTH];
			double tempRed[AADEPTH*AADEPTH];
			double tempGreen[AADEPTH*AADEPTH];
			double tempBlue[AADEPTH*AADEPTH];

			for (int aax = 0; aax < AADEPTH; aax++){
				for (int aay = 0; aay < AADEPTH; aay++){
					aa_index = aay*AADEPTH + aax;

					
					aliasedPixel(x, y, &xamnt, &yamnt, aax, aay, TECHNIQUE);

					dvec3 cam_ray_origin = camera.position;
					dvec3 cam_ray_direction = normalize(dvec3(camera.direction)*(0.5*HEIGHT*COTAN) + (dvec3(camera.right)*(xamnt)+(dvec3(camera.up)*(yamnt))));

					Ray cam_ray(cam_ray_origin, cam_ray_direction);

					//find intersection with each object in the scene
					int index_of_winning_object;
					double min_intersect;
					findMinIntersect(scene_objects, cam_ray, &min_intersect, &index_of_winning_object);
					if (TECHNIQUE == GAUSSIAN){
						sample_weight[aa_index] = exp((-0.5 / SIGMA_SQUARED)*((x - 0.5 * WIDTH + 0.5 - xamnt)*(x - 0.5 * WIDTH + 0.5 - xamnt) + (y - 0.5 * HEIGHT + 0.5 - yamnt)*(y - 0.5 * HEIGHT + 0.5 - yamnt)));
						total_weight += sample_weight[aa_index];
					}
					//background color
					if (index_of_winning_object == -1){
						tempRed[aa_index] = 0;//0.8;
						tempGreen[aa_index] = 0;//0.9;
						tempBlue[aa_index] = 0;//0.9;
					}
					else{
						// index corresponds to an object in our scene
						dvec3 intersection_position =
							cam_ray_origin + (cam_ray_direction * min_intersect);
						dvec3 intersection_ray_direction = cam_ray_direction;
						Color intersection_color =
							getColorAt(intersection_position, intersection_ray_direction, scene_objects, index_of_winning_object, light_sources, AMBIENT_LIGHT, RECURSION_DEPTH);

						tempRed[aa_index] = intersection_color.getColorRed();
						tempGreen[aa_index] = intersection_color.getColorGreen();
						tempBlue[aa_index] = intersection_color.getColorBlue();
					}	
				}
			}

			pixels[thisone].r = 0;
			pixels[thisone].g = 0;
			pixels[thisone].b = 0;

			for (int i = 0; i < AADEPTH*AADEPTH; i++){
				if (TECHNIQUE != GAUSSIAN){
					pixels[thisone].r += tempRed[i] / (AADEPTH*AADEPTH);
					pixels[thisone].g += tempGreen[i] / (AADEPTH*AADEPTH);
					pixels[thisone].b += tempBlue[i] / (AADEPTH*AADEPTH);
				}
				else{
					pixels[thisone].r += sample_weight[i] * tempRed[i] / total_weight;
					pixels[thisone].g += sample_weight[i] * tempGreen[i] / total_weight;
					pixels[thisone].b += sample_weight[i] * tempBlue[i] / total_weight;
				}
			}
		}
		if (x % 100 == 0)fprintf(stdout, "x = %d\n", x);
	}

	int i = 0;
	FILE* f;
	do{
		string name = "scene_" + to_string(i) + ".bmp";		
		fopen_s(&f, name.c_str(), "r");
		if (f == NULL) savebmp(name.c_str(), WIDTH, HEIGHT, dpi, pixels);
		else{
			i++;
			fclose(f);
		}
	} while (f != NULL);
	delete pixels;
	t2 = clock();
	float diff = ((float)t2 - (float)t1) / 1000;
	cout << diff << " seconds" << endl;
}


int main(int argc, char* argv[]){
	cout << "rendering..." << endl;
	
	Camera camera = Camera();
	Color pretty_orange(0.94, 0.75, 0.31, 0.9);
	Color pretty_blue(0.5, 0.5, 1, 0.5);

	// Initialise GLFW
	if (glInit() != 0) return -1;

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders("Shaders/StandardShading.vertexshader", "Shaders/StandardShading.fragmentshader");

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");


	// Load the texture
	GLuint Texture = loadDDS("Assets/uvtemplate.DDS");
	GLuint Texture2 = loadDDS("Assets/check.DDS");

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID = glGetUniformLocation(programID, "myTextureSampler");

	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJ("Assets/cube.obj", vertices, uvs, normals);

	double floor_size = 4;
	double roof_size = 4;
	double wall_size = 4;
	double off_set = -1;
	vec3 plane[6];
	plane[0] = vec3(-floor_size, off_set, -floor_size);
	plane[1] = vec3(-floor_size, off_set, +floor_size);
	plane[2] = vec3(+floor_size, off_set, -floor_size);
	plane[3] = vec3(-floor_size, off_set, +floor_size);
	plane[4] = vec3(+floor_size, off_set, +floor_size);
	plane[5] = vec3(+floor_size, off_set, -floor_size);

	vec2 uv[6];
	uv[0] = vec2(0, 0);
	uv[1] = vec2(0, 1);
	uv[2] = vec2(1, 0);
	uv[3] = vec2(0, 1);
	uv[4] = vec2(1, 1);
	uv[5] = vec2(1, 0);

	vec3 norm[6];
	norm[0] = vec3(0, 1, 0);
	norm[1] = vec3(0, 1, 0);
	norm[2] = vec3(0, 1, 0);
	norm[3] = vec3(0, 1, 0);
	norm[4] = vec3(0, 1, 0);
	norm[5] = vec3(0, 1, 0);

	for (int i = 0; i < 6; i++){
		vertices.push_back(plane[i]);
		uvs.push_back(uv[i]);
		normals.push_back(norm[i]);
	}

	
	vec3 roof[6];
	roof[1] = vec3(-roof_size, wall_size + off_set, -roof_size);
	roof[0] = vec3(-roof_size, wall_size + off_set, +roof_size);
	roof[2] = vec3(+roof_size, wall_size + off_set, -roof_size);
	roof[4] = vec3(-roof_size, wall_size + off_set, +roof_size);
	roof[3] = vec3(+roof_size, wall_size + off_set, +roof_size);
	roof[5] = vec3(+roof_size, wall_size + off_set, -roof_size);

	vec2 uv_roof[6];
	uv_roof[0] = vec2(0, 0);
	uv_roof[1] = vec2(0, 1);
	uv_roof[2] = vec2(1, 0);
	uv_roof[3] = vec2(0, 1);
	uv_roof[4] = vec2(1, 1);
	uv_roof[5] = vec2(1, 0);

	vec3 norm_roof[6];
	norm_roof[0] = vec3(0, -1, 0);
	norm_roof[1] = vec3(0, -1, 0);
	norm_roof[2] = vec3(0, -1, 0);
	norm_roof[3] = vec3(0, -1, 0);
	norm_roof[4] = vec3(0, -1, 0);
	norm_roof[5] = vec3(0, -1, 0);

	for (int i = 0; i < 6; i++){
		vertices.push_back(roof[i]);
		uvs.push_back(uv_roof[i]);
		normals.push_back(norm_roof[i]);
	}

	vec3 right_wall[6];
	right_wall[1] = vec3(+roof_size, wall_size + off_set, -roof_size);
	right_wall[0] = vec3(+roof_size, off_set, +roof_size);
	right_wall[2] = vec3(+roof_size, off_set, -roof_size);
	right_wall[4] = vec3(+roof_size, wall_size + off_set, -roof_size);
	right_wall[3] = vec3(+roof_size, wall_size + off_set, +roof_size);
	right_wall[5] = vec3(+roof_size, off_set, +roof_size);

	vec2 uv_right_wall[6];
	uv_right_wall[0] = vec2(0, 0);
	uv_right_wall[1] = vec2(0, 1);
	uv_right_wall[2] = vec2(1, 0);
	uv_right_wall[3] = vec2(0, 1);
	uv_right_wall[4] = vec2(1, 1);
	uv_right_wall[5] = vec2(1, 0);

	vec3 norm_right_wall[6];
	norm_right_wall[0] = vec3(-1, 0, 0);
	norm_right_wall[1] = vec3(-1, 0, 0);
	norm_right_wall[2] = vec3(-1, 0, 0);
	norm_right_wall[3] = vec3(-1, 0, 0);
	norm_right_wall[4] = vec3(-1, 0, 0);
	norm_right_wall[5] = vec3(-1, 0, 0);

	for (int i = 0; i < 6; i++){
		vertices.push_back(right_wall[i]);
		uvs.push_back(uv_right_wall[i]);
		normals.push_back(norm_right_wall[i]);
	}

	vec3 left_wall[6];
	left_wall[0] = vec3(-roof_size, wall_size + off_set, -roof_size);
	left_wall[1] = vec3(-roof_size, off_set, +roof_size);
	left_wall[2] = vec3(-roof_size, off_set, -roof_size);
	left_wall[3] = vec3(-roof_size, wall_size + off_set, -roof_size);
	left_wall[4] = vec3(-roof_size, wall_size + off_set, +roof_size);
	left_wall[5] = vec3(-roof_size, off_set, +roof_size);

	vec2 uv_left_wall[6];
	uv_left_wall[0] = vec2(0, 0);
	uv_left_wall[1] = vec2(0, 1);
	uv_left_wall[2] = vec2(1, 0);
	uv_left_wall[3] = vec2(0, 1);
	uv_left_wall[4] = vec2(1, 1);
	uv_left_wall[5] = vec2(1, 0);

	vec3 norm_left_wall[6];
	norm_left_wall[0] = vec3(1, 0, 0);
	norm_left_wall[1] = vec3(1, 0, 0);
	norm_left_wall[2] = vec3(1, 0, 0);
	norm_left_wall[3] = vec3(1, 0, 0);
	norm_left_wall[4] = vec3(1, 0, 0);
	norm_left_wall[5] = vec3(1, 0, 0);

	for (int i = 0; i < 6; i++){
		vertices.push_back(left_wall[i]);
		uvs.push_back(uv_left_wall[i]);
		normals.push_back(norm_left_wall[i]);
	}

	vec3 back_wall[6];
	back_wall[0] = vec3(-roof_size, wall_size + off_set, -roof_size);
	back_wall[2] = vec3(+floor_size, off_set, -floor_size);
	back_wall[1] = vec3(-floor_size, off_set, -floor_size);
	back_wall[4] = vec3(-roof_size, wall_size + off_set, -roof_size);
	back_wall[3] = vec3(+roof_size, wall_size + off_set, -roof_size);
	back_wall[5] = vec3(+floor_size, off_set, -floor_size);

	vec2 uv_back_wall[6];
	uv_back_wall[0] = vec2(0, 0);
	uv_back_wall[1] = vec2(0, 1);
	uv_back_wall[2] = vec2(1, 0);
	uv_back_wall[3] = vec2(0, 1);
	uv_back_wall[4] = vec2(1, 1);
	uv_back_wall[5] = vec2(1, 0);

	vec3 norm_back_wall[6];
	norm_back_wall[0] = vec3(0, 1, 0);
	norm_back_wall[1] = vec3(0, 1, 0);
	norm_back_wall[2] = vec3(0, 1, 0);
	norm_back_wall[3] = vec3(0, 1, 0);
	norm_back_wall[4] = vec3(0, 1, 0);
	norm_back_wall[5] = vec3(0, 1, 0);

	for (int i = 0; i < 6; i++){
		vertices.push_back(back_wall[i]);
		uvs.push_back(uv_back_wall[i]);
		normals.push_back(norm_back_wall[i]);
	}
	

	// Load it into a VBO
	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");
	vector<Object*> scene_objects;


	Material shiny_blue = Material(pretty_blue, 0.01, 0.98, 100);
	//NEWEST PART
	//here I push the new triangles into the scene_object vector
	for (int i = 0; i < (int)vertices.size(); i+=3){
		//scene_objects.push_back(new Triangle(vertices[i],vertices[i+1],vertices[i+2], pretty_blue));
		if (i < (int)vertices.size() - 30) scene_objects.push_back(new Triangle(vertices[i], vertices[i + 1], vertices[i + 2], Material(Color(1, 1, 1, 0), 0, 0.99, 100000)));
		if (i >= (int)vertices.size() - 30 && i < (int)vertices.size() - 24) scene_objects.push_back(new Triangle(vertices[i], vertices[i + 1], vertices[i + 2], Material(Color(1, 1, 1, 2), 0.2, 0.79, 1000)));
		if (i >= (int)vertices.size() - 24 && i < (int)vertices.size() - 18) scene_objects.push_back(new Triangle(vertices[i], vertices[i + 1], vertices[i + 2], Material(Color(0.7, 0.7, 0.7, 0.5), 0.2, 0.79, 1000)));
		if (i >= (int)vertices.size() - 18 && i < (int)vertices.size() - 12) scene_objects.push_back(new Triangle(vertices[i], vertices[i + 1], vertices[i + 2], Material(Color(1, 0, 0, 0.5), 0.2, 0.79, 1000)));
		if (i >= (int)vertices.size() - 12 && i < (int)vertices.size() - 6) scene_objects.push_back(new Triangle(vertices[i], vertices[i + 1], vertices[i + 2], Material(Color(0, 0, 1, 0.5), 0.2, 0.79, 1000)));
		if (i >= (int)vertices.size() - 6) scene_objects.push_back(new Triangle(vertices[i], vertices[i + 1], vertices[i + 2], Material(Color(0, 1, 0, 0.5), 0.2, 0.79, 1000)));
	}
	int i = 0;
	do{

		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS && i == 0){			
			camera;
			rayTrace(scene_objects, camera);
			i++;
		}

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Compute the MVP matrix from keyboard and mouse input
		computeMatricesFromInputs(&camera, window);
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

		glm::vec3 lightPos = glm::vec3(4, 4, 4);
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to user Texture Unit 0
		glUniform1i(TextureID, 0);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// Draw the triangles !
		glDrawArrays(GL_TRIANGLES, 0, vertices.size()-12);
		//glDrawArrays(GL_TRIANGLES, 0, vertices.size());
		// Bind our texture in Texture Unit 0
		//glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture2);
		// Set our "myTextureSampler" sampler to user Texture Unit 0
		//glUniform1i(TextureID, 0);
		glDrawArrays(GL_TRIANGLES, vertices.size() - 12, vertices.size());

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(window) == 0);

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteProgram(programID);
	//glDeleteTextures(1, &Texture);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
	
	getchar();
	return 0;
}