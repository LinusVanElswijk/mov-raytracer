#ifndef _SHARED_CONSTANTS_H
#define _SHARED_CONSTANTS_H
#include <glm\glm.hpp>
#include <Primitives\vect.h>
using glm::vec3;
const double ACCURACY = 0.000001;

static Vect vecToVect(vec3 v){
	return Vect(v.x, v.y, v.z);
}

static vec3 vecToVect(Vect v){
	return vec3(v.getVectX(), v.getVectY(), v.getVectZ());
}

#endif